public class Task5050 {
    public static void main(String[] args) {
        System.out.print("ST1");
        String sT1 = "DCresource: JavaScript Exercises";
        char n1 = 'e';
        int demSt1 = 0;
        sT1 = sT1.toLowerCase();
        n1 = Character.toLowerCase(n1);
        for (int i = 0; i < sT1.length(); i++) {
            if (sT1.charAt(i) == n1) {
                demSt1++;
            }
        }

        System.out.println("Số lần xuất hiện của ký tự '" + n1 + "' trong chuỗi là: " + demSt1);

        System.out.print("ST2");
        String sT2 = " dcresource ";
        String chuoiDaLoaiBoKyTuTrang = sT2.trim();
        System.out.println(chuoiDaLoaiBoKyTuTrang);

        System.out.print("ST3");
        String sT3 = "The quick brown fox jumps over the lazy dog";
        String chuoiConsT3 = "the";

        String chuoiDaLoaiBo = sT3.replaceAll(chuoiConsT3, "");

        System.out.println(sT3);
        System.out.println(chuoiDaLoaiBo);

        System.out.print("ST4");
        String chuoiTruoc = "JS PHP PYTHON";
        String chuoiSau = "PYTHON";

        boolean ketQua = chuoiTruoc.endsWith(chuoiSau);
        if (ketQua) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        String chuoiTruoc2 = "JS PHP PYTHON";
        String chuoiSau2 = "JS";

        boolean ketQua2 = chuoiTruoc2.endsWith(chuoiSau2);
        if (ketQua2) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        System.out.print("ST5");
        String chuoiTruocSt5 = "abcd";
        String chuoiSauSt5 = "AbcD";
        boolean ketQuaSt5 = chuoiTruocSt5.equalsIgnoreCase(chuoiSauSt5);

        if (ketQuaSt5) {
            System.out.println("Hai chuỗi giống nhau");
        } else {
            System.out.println("Hai chuỗi khác nhau");
        }

        String chuoiTruoc2St5 = "ABCD";
        String chuoiSau2St5 = "Abce";
        boolean ketQua2St5 = chuoiTruoc2St5.equalsIgnoreCase(chuoiSau2St5);
        if (ketQua2St5) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

        System.out.print("ST6");
        String sT6 = "Js STRING EXERCISES";
        int n = 1;
        char kyTu = sT6.charAt(n - 1); // Lấy ký tự tại vị trí n
        boolean isVietHoa = Character.isUpperCase(kyTu);

        if (isVietHoa) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

        String sT62 = "Js STRING EXERCISES";
        int n2 = 2;
        char kyTu2 = sT62.charAt(n2 - 1);
        boolean vietHoa = Character.isUpperCase(kyTu2);
        if (vietHoa) {
            System.out.println("true");
        } else {
            System.out.println("False");
        }

        System.out.println("ST7");
        String sT7 = "Js STRING EXERCISES";
        int nSt7 = 1;
        char kyTuSt7 = sT7.charAt(nSt7 - 1); // Lấy ký tự tại vị trí n
        boolean isVietThuong = Character.isLowerCase(kyTuSt7);

        if (isVietThuong) {
            System.out.println("False");
        } else {
            System.out.println("True");
        }
        String sT72 = "Js STRING EXERCISES";
        int nSt72 = 2;
        char kyTuSt72 = sT72.charAt(nSt72 - 1); // Lấy ký tự tại vị trí n
        boolean isVietThuong2 = Character.isLowerCase(kyTuSt72);

        if (isVietThuong2) {
            System.out.println("False");
        } else {
            System.out.println("True");
        }

        System.out.println("ST8");

        String sT8 = "js string exercises";
        String conSt8 = "js";
        boolean batDau = sT8.startsWith(conSt8);
        if (batDau) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        System.out.println("ST9");
        String sT9 = "abc";
        String conSt9 = "";
        boolean chuoiSt9 = sT9.isEmpty();
        if (chuoiSt9) {
            System.out.println("True");
        } else {
            System.out.println("false");
        }
        boolean chuoiSt92 = conSt9.isEmpty();
        if (chuoiSt92) {
            System.out.println("true");
        } else {
            System.out.println("False");
        }

        System.out.println("ST10");

        String sT10 = "AaBbc";
        StringBuilder stringBuilder = new StringBuilder(sT10);
        String chuoiDaoNguoc = stringBuilder.reverse().toString();

        System.out.println(chuoiDaoNguoc);
    }
}
