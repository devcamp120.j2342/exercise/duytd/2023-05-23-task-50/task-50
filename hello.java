public class hello {
    public static void main(String[] args) throws Exception {
        System.out.println("In ra tổng 100 số tự nhiên");
        int sum100 = sumNumbersV1();

        System.out.println(sum100);

        System.out.println("ST3: ");

        int[] number1 = { 1, 5, 10 };

        int[] number2 = { 1, 2, 3, 5, 7, 9 };

        System.out.println(sumNumbersV2(number1));
        System.out.println(sumNumbersV2(number2));

        System.out.println("ST4: ");

        printHello(24);
        printHello(99);

    };

    public static int sumNumbersV1() {
        int sum = 0;

        for (int i = 0; i < 100; i++) {
            // sum = sum + i;
            sum += i;
        }

        return sum;
    };

    public static int sumNumbersV2(int[] arrayNumber) {
        int sum = 0;

        for (int i = 0; i < arrayNumber.length; i++) {
            sum += arrayNumber[i];
        }

        return sum;
    };
    public static void printHello(int number) {
        if(number <= 0) {
            System.out.println("So da cho nho hon hoac bang 0");
        } else {
            int du = number % 2;

            if(du == 0) {
                System.out.println("Đây là số chẵn");
            } else {
                System.out.println("Đây là số lẻ");
            }
        }
    }

}
