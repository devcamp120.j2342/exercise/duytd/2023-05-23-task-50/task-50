import java.util.*;
public class Task5040 {
    public static void main(String[] args) {
        System.out.println("ST1");
        int x = -4;
        int y = 7;

        // Xác định kích thước của mảng
        int size = Math.abs(y - x) + 1;

        // Khởi tạo mảng
        int[] mang = new int[size];

        if (x <= y) {
            for (int i = 0; i < size; i++) {
                mang[i] = x++;
            }
        } else {
            for (int i = 0; i < size; i++) {
                mang[i] = x--;
            }
        }

        // In mảng
        for (int i = 0; i < size; i++) {
            System.out.print(mang[i] + " ");
        }

        System.out.println("ST2");
        int[] mang1 = { 1, 2, 3 };
        int[] mang2 = { 100, 2, 1, 10 };

        // Gộp hai mảng thành một mảng duy nhất
        int[] mangGop = gopMang(mang1, mang2);

        // In mảng gộp
        for (int i = 0; i < mangGop.length; i++) {
            System.out.print(mangGop[i] + " ");
        }

        System.out.println("ST3");
        int[] mangSt3 = { 1, 2, 1, 4, 5, 1, 1, 3, 1 };
        int n = 1;

        int dem = demSoLanXuatHien(mangSt3, n);
        System.out.println("Số lần xuất hiện của " + n + " trong mảng là: " + dem);

        System.out.println("ST4");
        int[] mangSt4 = { 1, 2, 3, 4, 5, 6 };

        int tong = tinhTong(mangSt4);
        System.out.println("Tổng các phần tử trong mảng là: " + tong);

        System.out.println("ST5");

        int[] mangSt5 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        int[] mangSoChan = taoMangSoChan(mangSt5);

        System.out.print("Mảng các số chẵn: ");
        for (int i = 0; i < mangSoChan.length; i++) {
            System.out.print(mangSoChan[i] + " ");
        }

        System.out.println("ST6");
        int[] mangSo1 = { 1, 0, 2, 3, 4 };
        int[] mangSo2 = { 3, 5, 6, 7, 8, 13 };
        int[] mangTong = taoMangTong(mangSo1, mangSo2);

        System.out.print("Mảng tổng: ");
        for (int i = 0; i < mangTong.length; i++) {
            System.out.print(mangTong[i] + " ");
        }

        System.out.println("ST7");
        int[] mangSt6 = { 1, 2, 3, 1, 5, 1, 4, 6, 3, 4 };
        int[] mangMoi = taoMangDuyNhat(mangSt6);
        System.out.print("Mảng mới chứa các phần tử duy nhất: ");
        for (int i = 0; i < mangMoi.length; i++) {
            System.out.print(mangMoi[i] + " ");
        }

        System.out.println("ST8");
        int[] mangSo1St8 = { 1, 2, 3 };
        int[] mangSo2St8 = { 100, 2, 1, 10 };

        int[] mangKetQua = layPhanTuKhongTrungNhau(mangSo1St8, mangSo2St8);

        System.out.print("Các phần tử không trùng nhau: ");
        for (int i = 0; i < mangKetQua.length; i++) {
            System.out.print(mangKetQua[i] + " ");
        }

        System.out.println("ST9");
        int[] mangSt9 = {5, 2, 8, 1, 9, 3};
        // sắp xếp dãy số theo thứ tự giảm dần
        sortDESC(mangSt9);
        System.out.println("Dãy số được sắp xếp giảm dần: ");
        show(mangSt9);

        System.out.println("ST10");
        int[] mangSt10 = {10, 20, 30, 40, 50};
        int xSt10 = 0;
        int ySt10 = 2;

        // Kiểm tra x và y có hợp lệ
        if (xSt10 >= 0 && xSt10 < mangSt10.length && ySt10 >= 0 && ySt10 < mangSt10.length) {
    
            int temp = mangSt10[xSt10];
            mangSt10[xSt10] = mangSt10[ySt10];
            mangSt10[ySt10] = temp;

            // In mảng sau khi đổi chỗ
            System.out.println("Mảng sau khi đổi chỗ:");
            for (int i = 0; i < mangSt10.length; i++) {
                System.out.print(mangSt10[i] + " ");
            }
        } else {
            System.out.println("Vị trí x và y không hợp lệ.");
        }
    }
    //ST10

    
    //ST 9
    public static void sortDESC(int [] arr) {
        int temp = arr[0];
        for (int i = 0 ; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
    }
    public static void show(int [] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
    // ST 8
    // Phương thức lấy các phần tử không trùng nhau của hai mảng
    public static int[] layPhanTuKhongTrungNhau(int[] mang1, int[] mang2) {
        Set<Integer> set = new HashSet<>();
        Set<Integer> ketQuaSet = new HashSet<>();

        // Thêm các phần tử của mảng 1 vào set
        for (int i = 0; i < mang1.length; i++) {
            set.add(mang1[i]);
        }

        for (int i = 0; i < mang2.length; i++) {
            if (set.contains(mang2[i])) {
                set.remove(mang2[i]);
            } else {
                ketQuaSet.add(mang2[i]);
            }
        }

        // Tạo mảng kết quả từ set
        int[] mangKetQua = new int[ketQuaSet.size()];
        int index = 0;
        for (int num : ketQuaSet) {
            mangKetQua[index] = num;
            index++;
        }

        return mangKetQua;
    }

    // ST7
    // Phương thức tạo mảng mới chỉ chứa các phần tử duy nhất
    public static int[] taoMangDuyNhat(int[] mang) {
        Set<Integer> set = new HashSet<>();

        for (int i = 0; i < mang.length; i++) {
            set.add(mang[i]);
        }

        // Tạo mảng mới có kích thước bằng số lượng phần tử duy nhất
        int[] mangMoi = new int[set.size()];

        int index = 0;
        // Sao chép các phần tử từ HashSet vào mảng mới
        for (int num : set) {
            mangMoi[index] = num;
            index++;
        }

        return mangMoi;
    }

    // ST6
    // Phương thức tạo mảng tổng từ hai mảng ban đầu
    public static int[] taoMangTong(int[] mang1, int[] mang2) {
        int doDai = Math.min(mang1.length, mang2.length);

        int[] mangTong = new int[doDai]; // Khởi tạo mảng tổng

        for (int i = 0; i < doDai; i++) {
            mangTong[i] = mang1[i] + mang2[i]; // Tính tổng phần tử tương ứng
        }

        return mangTong;
    }

    // ST5
    // Phương thức tạo mảng các số chẵn từ mảng ban đầu
    public static int[] taoMangSoChan(int[] mang) {
        int demSoChan = 0;

        for (int i = 0; i < mang.length; i++) {
            if (mang[i] % 2 == 0) {
                demSoChan++;
            }
        }

        // Khởi tạo mảng mới với kích thước là số lượng số chẵn
        int[] mangSoChan = new int[demSoChan];

        // Thêm các số chẵn vào mảng mới
        int index = 0;
        for (int i = 0; i < mang.length; i++) {
            if (mang[i] % 2 == 0) {
                mangSoChan[index] = mang[i];
                index++;
            }
        }

        return mangSoChan;
    }

    // ST4
    // Phương thức tính tổng các phần tử trong mảng
    public static int tinhTong(int[] mang) {
        int tong = 0;

        for (int i = 0; i < mang.length; i++) {
            tong += mang[i];
        }

        return tong;
    }

    // ST3
    // Phương thức đếm số lần xuất hiện của một phần tử trong mảng
    public static int demSoLanXuatHien(int[] mang, int n) {
        int dem = 0;

        for (int i = 0; i < mang.length; i++) {
            if (mang[i] == n) {
                dem++;
            }
        }

        return dem;
    }

    // ST2
    public static int[] gopMang(int[] mang1, int[] mang2) {
        Set<Integer> set = new HashSet<>();

        // Thêm phần tử từ mảng 1 vào tập hợp
        for (int i = 0; i < mang1.length; i++) {
            set.add(mang1[i]);
        }

        // Thêm phần tử từ mảng 2 vào tập hợp
        for (int i = 0; i < mang2.length; i++) {
            set.add(mang2[i]);
        }

        // Chuyển tập hợp thành mảng
        int[] mangGop = new int[set.size()];
        int index = 0;
        for (int num : set) {
            mangGop[index++] = num;
        }

        return mangGop;
    }
}
