import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
public class Array {
    
    public static void main(String[] args) {
        System.out.println("ST1");
        Object obj1 = "Devcamp";
        Object obj2 = new int[]{1, 2, 3};
        
        System.out.println(isArray(obj1)); // false
        System.out.println(isArray(obj2)); // true

        System.out.println("ST2");
        int[] array1 = {1, 2, 3, 4, 5, 6};
        int[] array2 = {1, 2, 3, 4, 5, 6};

        int n1 = 3;
        int n2 = 6;

        Integer element1 = getElementAtIndex(array1, n1);
        Integer element2 = getElementAtIndex(array2, n2);

        System.out.println(element1); // Output: 4
        System.out.println(element2); // Output: null (vì chỉ số vượt quá kích thước của mảng)

        System.out.println("ST3");

        int[] array = {3,8,7,6,5,-4,-3,2,1};
        Arrays.sort(array);
        
        for (int number : array) {
            System.out.print(number + " ");
        }

        System.out.println("ST4");

        int[] arraySt41 = {1, 2, 3, 4, 5, 6};
        int[] arraySt42 = {1, 2, 3, 4, 5, 6};
        int nSt41 = 3;
        int nSt42 = 7;

        int index1 = findElementIndex(arraySt41, nSt41);
        int index2 = findElementIndex(arraySt42, nSt42);

        System.out.println(index1); // Output: 2
        System.out.println(index2); // Output: -1 (không tìm thấy phần tử)

        System.out.println("ST5");

        int[] arraySt51 = {1, 2, 3};
        int[] arraySt52 = {4, 5, 6};

        int[] newArray = new int[arraySt51.length + arraySt52.length];
        
        System.arraycopy(arraySt51, 0, newArray, 0, arraySt51.length);
        System.arraycopy(arraySt52, 0, newArray, arraySt51.length, arraySt52.length);
        
        // In mảng mới
        for (int number : newArray) {
            System.out.print(number + " ");
        };

        System.out.println("ST6");
        Object[] input = {Double.NaN, 0, 15, false, -22, "html", "develop", 47, null};

        Object[] output = filterElements(input);

        for (Object element : output) {
            System.out.print(element + " ");
        };

        System.out.println("ST7");
        int[] arraySt71 = {2, 5, 9, 6};
        int nSt71 = 5;
        int[] arraySt72 = {2, 9, 6};
        int nSt72 = 5;
        int[] arraySt73 = {2, 5, 9, 6};
        int nSt73 = 6;
        int[] newArraySt71 = removeElements(arraySt71, nSt71);
        int[] newArraySt72 = removeElements(arraySt72, nSt72);
        int[] newArraySt73 = removeElements(arraySt73, nSt73);

        System.out.println(Arrays.toString(newArraySt71)); // Output: [2, 9, 6]
        System.out.println(Arrays.toString(newArraySt72)); // Output: [2, 9, 6]
        System.out.println(Arrays.toString(newArraySt73)); // Output: [2, 5, 9]

        System.out.println("ST8");

        int[] arraySt81 = {1,2,3,4,5,6,7,8,9};
        int[] arraySt82 = {1,2,3,4,5,6,7,8,9};
        int[] arraySt83 = {1,2,3,4,5,6,7,8,9};
        int randomElement1 = getRandomElement(arraySt81);
        int randomElement2 = getRandomElement(arraySt82);
        int randomElement3 = getRandomElement(arraySt83);

        System.out.println("Phần tử ngẫu nhiên trong arraySt81: " + randomElement1);
        System.out.println("Phần tử ngẫu nhiên trong arraySt82: " + randomElement2);
        System.out.println("Phần tử ngẫu nhiên trong arraySt83: " + randomElement3);

        System.out.println("ST9");
        int x = 6;
        int y = 0;
        int[] arr = new int[x];
        Arrays.fill(arr, y);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        };

        int x1 = 4;
        int y1 = 11;
        int[] arr1 = new int[x1];
        Arrays.fill(arr1, y1);
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }

        System.out.println("ST10");
        int xSt10 = 1;
        int ySt10 = 4;
        
        int[] arrSt10 = new int[ySt10];
        
        for (int i = 0; i < arrSt10.length; i++) {
            arrSt10[i] = xSt10 + i;
        }
        
        for (int i = 0; i < arrSt10.length; i++) {
            System.out.print(arrSt10[i] + " ");
        }

        int xSt101 = -6;
        int ySt101 = 4;

        int[] arrSt101 = new int[ySt101];
        for (int i = 0; i < arrSt101.length; i++) {
            arrSt101[i] = xSt101 + i;
        }
        
        for (int i = 0; i < arrSt101.length; i++) {
            System.out.print(arrSt101[i] + " ");
        }

    };

    public static int getRandomElement(int[] array) {
        Random random = new Random();
        int randomIndex = random.nextInt(array.length);
        return array[randomIndex];
    }

    public static int[] removeElements(int[] array, int n) {
        List<Integer> result = new ArrayList<>();

        for (int element : array) {
            if (element != n) {
                result.add(element);
            }
        }

        // Chuyển đổi List thành mảng
        int[] newArray = new int[result.size()];
        for (int i = 0; i < result.size(); i++) {
            newArray[i] = result.get(i);
        }

        return newArray;
    }

    public static Object[] filterElements(Object[] array) {
        List<Object> result = new ArrayList<>();

        for (Object element : array) {
            if (isValueElement(element)) {
                result.add(element);
            }
        }

        return result.toArray();
    }

    public static boolean isValueElement(Object element) {
        return (element instanceof Number) || (element instanceof String);
    }

    public static boolean isArray(Object obj) {
        return obj != null && obj.getClass().isArray();
    };

    public static Integer getElementAtIndex(int[] array, int index) {
        if (index >= 0 && index < array.length) {
            return array[index];
        }
        return null;
    };
    public static int findElementIndex(int[] array, int n) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                return i;
            }
        }
        return -1; // Trả về -1 nếu không tìm thấy phần tử
    }
}
