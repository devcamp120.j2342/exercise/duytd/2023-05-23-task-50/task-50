public class Task5030{
    public static void main(String[] args) {
        System.out.println("ST1");
        Object input = "Devcamp";
        boolean isString = input instanceof String;
        System.out.println(isString); // Kết quả: true

        Object input2 = 1;
        boolean isString2 = input2 instanceof String;
        System.out.println(isString2); // Kết quả: false

        System.out.println("ST2");

        String chuoi = "Robin Singh";
        int soThu = 4; // Vị trí phần tử thứ n
        String phanTuThuN = chuoi.substring(0, soThu); // Cắt từ vị trí n đến vị trí n
        System.out.println(phanTuThuN);

        System.out.println("ST3");

        String chuoiSt3 = "Robin Singh";
        String[] mangTu = chuoiSt3.split(" "); // Tách chuỗi thành mảng các từ dựa trên dấu cách

        // In mảng các từ
        for (String tu : mangTu) {
            System.out.println(tu);
        }

        System.out.println("ST4");

        String sT4 = "Robin Singh from USA";
        
        String formattedString = sT4.toLowerCase().trim();
        
        formattedString = formattedString.replace(" ", "-");
        
        System.out.println(formattedString); // Kết quả: "robin-singh-from-usa"

        System.out.println("ST5");
        String chuoiSt5 = "JavaScript Exercises";
        
        String chuoiVietHoa = chuoiSt5.substring(0, 1).toUpperCase() + chuoiSt5.substring(1);
        formattedString = chuoiVietHoa.replace(" ", "");
        System.out.println(formattedString); 

        System.out.println("ST6");

        String chuoiSt6 = "js string exercises";

        String[] mangTuSt6 = chuoiSt6.split(" "); //tách chuổi thành các mảng từ
        StringBuilder ketQua = new StringBuilder();
        for(int i = 0; i < mangTuSt6.length; i++){ //xử lý từ trong mảng
            String tuVietHoa = mangTuSt6[i].substring(0, 1).toUpperCase() + mangTuSt6[i].substring(1);
            ketQua.append(tuVietHoa).append(" ");
        }
        String ketQuaChuoi = ketQua.toString().trim();//xóa khoảng trắng thừa ở chuổi kết quả
        System.out.println(ketQuaChuoi); // Kết quả: "Js String Exercises"

        System.out.println("ST7");
        String tu = "Ha!";
        int n = 3;
        
        StringBuilder ketQuaSt7 = new StringBuilder();
        for (int i = 1; i <= n; i++) {
            ketQuaSt7.append(tu);
            
            // Thêm khoảng trắng giữa các lần lặp (trừ lần cuối)
            if (i < n) {
                ketQuaSt7.append(" ");
            }
        }
        
        System.out.println(ketQuaSt7.toString()); // Kết quả: "'Ha!' 'Ha!' 'Ha!'"

        System.out.println("ST8");

        String chuoiSt8 = "dcresource";
        int nSt8 = 3;
        int chieuDaiChuoi = chuoiSt8.length();
        int soPhanTu = (int) Math.ceil((double) chieuDaiChuoi / nSt8);
        String[] mangPhanTu = new String[soPhanTu];
        
        for (int i = 0; i < soPhanTu; i++) {
            int startIndex = i * nSt8;
            int endIndex = Math.min(startIndex + nSt8, chieuDaiChuoi);
            mangPhanTu[i] = chuoiSt8.substring(startIndex, endIndex);
        }
        
        // In mảng các phần tử
        for(int i = 0; i < mangPhanTu.length; i++){
            System.out.println(mangPhanTu[i]);
        }

        System.out.println("ST9");
        String chuoiSt9 = "The quick brown fox jumps over the lazy dog";
        String chuoiCon = "the";
        
        int dem = 0;
        int startIndex = 0;
        
        while (true) {
            int index = chuoiSt9.toLowerCase().indexOf(chuoiCon.toLowerCase(), startIndex);
            if (index == -1) {
                break;
            }
            
            dem++;
            startIndex = index + chuoiCon.length();
        }
        
        System.out.println("Số lần xuất hiện của chuỗi con là: " + dem);

        System.out.println("ST10");
        String chuoiSt10 = "0000";
        String chuoiMoi = "123";
        
        
        String ketQuaSt10 = chuoiSt10.substring(0, chuoiSt10.length() - 2) + chuoiMoi;
        System.out.println("Kết quả: " + ketQuaSt10);
    }
}
